from pi_pico_lcd_rgb_grove import *
from machine import Pin, I2C, PWM
from time import sleep
import random
i2c0 = I2C(0, scl=Pin(9), sda=Pin(8), freq=400000)
lcd = Lcd_rgb_grove(i2c0)
pwm = PWM(Pin(27))
pwm.freq(1000)
def poser():
    lcd.clear()
    lcd.color(255, 255, 255)
    lcd.setCursor(3, 0)
    lcd.write('Poser etui')
    pwm.duty_u16(0)
def chargement():
    lcd.clear()
    lcd.color(0, 0, 255)
    lcd.setCursor(2, 0)
    lcd.write('Patientez...')
    pwm.duty_u16(0)
def erreur():
    lcd.clear()
    lcd.color(255, 0, 0)
    lcd.setCursor(0, 0)
    lcd.write('Il manque ')
    x=random.randint(1,50)
    lcd.write(x)
    lcd.setCursor(0, 1)
    lcd.write('cartouche(s) !')
    pwm.duty_u16(500)
def valide():
    lcd.clear()
    lcd.color(0, 255, 0)
    lcd.setCursor(1, 0)
    lcd.write('Pesee valide !')
    pwm.duty_u16(0)
try:
    print("start")
    while True:
        poser()
        sleep(3)
        chargement()
        sleep(3)
        erreur()
        sleep(3)
        valide()
        sleep(3)
except KeyboardInterrupt:
    print("end")