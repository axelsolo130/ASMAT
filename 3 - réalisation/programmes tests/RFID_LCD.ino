#include <SPI.h>
#include <MFRC522.h>
#include <Wire.h>
#include "rgb_lcd.h"  // Inclure la bibliothèque pour l'écran Grove LCD RGB

// Affectation des broches
#define RST_PIN 9
#define SS_PIN 10

MFRC522 mfrc522(SS_PIN, RST_PIN);
rgb_lcd lcd; // Déclaration de l'objet LCD

void setup() {
  // Initialisation du Module RFID
  Serial.begin(9600);
  while (!Serial);
  SPI.begin();
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial(); // Affichage des données de la bibliothèque
  Serial.println(F("Scan PICC to see UID, type, and data blocks..."));

  // Initialisation de l'écran LCD
  lcd.begin(16, 2);
  lcd.setRGB(0, 255, 0); // Couleur par défaut de l'écran LCD
}

void loop() {
  // Attente d'une carte RFID
  if (!mfrc522.PICC_IsNewCardPresent()) {
    return;
  }
  
  // Récupération des informations de la carte RFID
  if (!mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  
  // Affichage de l'UID sur l'écran LCD
  lcd.clear(); // Effacer l'écran
  lcd.setCursor(0, 0); // Déplacer le curseur à la première ligne
  lcd.print("UID Value: "); // Afficher le texte
  for (byte i = 0; i < mfrc522.uid.size; ++i) {
    if (mfrc522.uid.uidByte[i] < 0x10)
      lcd.print("0");
    lcd.print(mfrc522.uid.uidByte[i], HEX);
  }
}
