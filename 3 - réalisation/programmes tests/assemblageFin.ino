#include "HX711.h"
#include <SPI.h>
#include <MFRC522.h>
#include <rgb_lcd.h>
#include <Servo.h>


const int LOADCELL_DOUT_PIN_1 = 4;
const int LOADCELL_SCK_PIN_1 = 3;
const int LOADCELL_DOUT_PIN_2 = 7;
const int LOADCELL_SCK_PIN_2 = 6;
int inputPin = 3;               // Connecter le capteur à la broche d'entrée 3
#define RST_PIN 9
#define SS_PIN 10
#define BUZZER_PIN 4

MFRC522 mfrc522(SS_PIN, RST_PIN);
HX711 scale1;
HX711 scale2;
rgb_lcd lcd;

const float calibration_factor1 = -7050; //-7050 cette variable à régler selon le capteur de poids
const float calibration_factor2 = -7050; //-7050 cette variable à régler selon le capteur de poids

int index = 0;

String status;

void setup() {
  Serial.begin(9600);
  pinMode(inputPin, INPUT);     // Déclarer le microrupteur comme entrée
  while (!Serial);
  SPI.begin();
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial(); // Affichage des données de la bibliothèque

  lcd.begin(16, 2); // Initialiser l'écran avec 16 colonnes et 2 lignes
  lcd.setRGB(0, 255, 0); // Définir la couleur de fond (vert)

  scale1.begin(LOADCELL_DOUT_PIN_1, LOADCELL_SCK_PIN_1);
  scale2.begin(LOADCELL_DOUT_PIN_2, LOADCELL_SCK_PIN_2);

  scale1.set_scale();
  scale2.set_scale();

  scale1.tare(); // Réinitialise la balance à 0
  scale2.tare(); // Réinitialise la balance à 0

  long zero_factor1 = scale1.read_average(); // Lecture de référence pour l'échelle 1
  long zero_factor2 = scale2.read_average(); // Lecture de référence pour l'échelle 2

}

float lecture_poids(HX711 scale, float calibration_factor) {
  scale.set_scale(calibration_factor); // Ajuste selon ce facteur d'étalonnage
  return scale.get_units();
}

void affichage_info(float poids_moyen) {
}

// ... (le reste du code est inchangé)

void loop() {
  // Lecture des valeurs de poids à partir des capteurs de pesage
  float poids1 = lecture_poids(scale1, calibration_factor1);
  float poids2 = lecture_poids(scale2, calibration_factor2);

  float poids_moyen = (poids1 + poids2) / 2.0 - 0.220; // Calculer le poids moyen des deux capteurs

  int cartouches_manquantes = 10;

  // Effacement de l'écran LCD et positionnement du curseur au début
  lcd.clear();
  lcd.setCursor(0, 0);

  // Vérification de la présence d'une carte RFID
  if (mfrc522.PICC_IsNewCardPresent()) {
    // Lecture du numéro de série de la carte RFID
    if (mfrc522.PICC_ReadCardSerial()) {

      String UID = "";
      for (byte i = 0; i < mfrc522.uid.size; ++i) {
        UID += String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
        UID += String(mfrc522.uid.uidByte[i], HEX);
      }
      Serial.println(" "+ UID);



      if (poids_moyen <= 0) {
        cartouches_manquantes = 10; // Nombre de cartouches manquantes si le poids est nul ou négatif
      } else {
        cartouches_manquantes = int(10 - poids_moyen / 0.162); // Calculer le nombre de cartouches manquantes à chaque multiple de 0.200 kg
        status = "manque " + String(cartouches_manquantes) + " cartouches";
        digitalWrite(BUZZER_PIN, HIGH);
        delay(1000);
        digitalWrite(BUZZER_PIN, LOW);
        Serial.println(cartouches_manquantes);
      }

      if (cartouches_manquantes <= 0){
      Serial.println("il y a tout");
      digitalWrite(BUZZER_PIN, LOW);
      status = "tout est OK";
      }    
        if (Serial.available() > 0) { 
    String data = Serial.readStringUntil('\n');
    String state = Serial.readStringUntil('\n');

    lcd.print("Bonjour ");
    lcd.print(data);
    lcd.setCursor(0, 1);
    lcd.print(status);
    
    if (data == "locked" || data == "unlocked") {
        // Lire la ligne suivante du port série
        String nextState = Serial.readStringUntil('\n');

        //if state = unlocked ;
        //servo moteur à 0
        //else 
        //servo moteur à 90 pendant 5 sec puis retour à 0
        } 
    delay(1000);
    }
  }
  }

     int val = digitalRead(inputPin);  // Lire la valeur d'entrée
  if (val == LOW) {                // Vérifier si l'entrée est HIGH
  } 
  else {
    Serial.println("+");
  }
}