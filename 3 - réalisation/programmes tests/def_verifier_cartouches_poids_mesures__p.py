def verifier_cartouches(poids_mesures, poids_reference, poids_attendu, marge_erreur):
    poids_total_mesure = sum(poids_mesures)

    poids_min_attendu = poids_attendu - marge_erreur
    poids_max_attendu = poids_attendu + marge_erreur

    if poids_min_attendu <= poids_total_mesure <= poids_max_attendu:
        return "Toutes les cartouches sont présentes."
    elif poids_total_mesure > poids_max_attendu:
        cartouches_supplementaires = int((poids_total_mesure - poids_attendu) / poids_reference)
        return f"Il y a {cartouches_supplementaires} cartouches supplémentaires."
    else:
        cartouches_manquantes = int((poids_attendu - poids_total_mesure) / poids_reference)
        return f"Il manque {cartouches_manquantes} cartouches."


# Poids de référence pour une cartouche de 5.56 mm (en kg)
poids_reference = 0.00682

# Poids attendu pour les 50 cartouches de 5.56 mm (en kg)
poids_attendu = 0.341

# Marge d'erreur de mesure (en kg)
marge_erreur = 0.005

# Poids mesurés des cartouches de 5.56 mm (en kg)
poids_mesures = [0.341]

resultat = verifier_cartouches(poids_mesures, poids_reference, poids_attendu, marge_erreur)
print(resultat)