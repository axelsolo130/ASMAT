import sqlite3
import datetime
import serial
from time import sleep

port = 'COM5'
baud_rate = 9600
ser = serial.Serial(port, baud_rate)

def time():
    time = datetime.datetime.now()
    time = time.strftime("%Y-%m-%d %H:%M:%S")
    return time

def db_stock(distro_uid, parameter):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT distro_stock FROM distro WHERE distro_uid=?", (distro_uid,))
    result=c.fetchone()[0]
    if parameter == 1:
        result+=1
    else:
        result-=1
    print("total de mun:", result)
    c.execute("UPDATE distro SET distro_stock=? WHERE distro_uid=?", (result, distro_uid,))
    conn.commit()
    c.close()
    conn.close()

def db_mun(card_number, ammunition_type, ammunition_number, distro_uid):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT uid FROM whitelist WHERE card_number=?", (card_number,))
    uid = c.fetchone()[0]
    c.execute("SELECT distro_stock FROM distro WHERE distro_uid=?", (distro_uid,))
    result=c.fetchone()[0]
    if uid and result>0:
        c.execute("SELECT last_name FROM whitelist WHERE uid=?", (uid,))
        last_name = c.fetchone()[0]
        print("Nom de la personne:", last_name)  # Affichage sur le shell
        ser.write(last_name.encode())  # Envoi du nom au port série
        ser.write(b'\n')
        c.execute("SELECT state FROM whitelist WHERE uid=?", (uid,))
        state=c.fetchone()[0]
        ser.write(state.encode())
        ser.write(b'\n')
        print(state)
        c.execute("SELECT CAST(ammunition_number AS INTEGER) FROM log WHERE uid=?", (uid,))
        ammunition_number_t = c.fetchall()
        t = sum(int(ammunition_number[0]) for ammunition_number in ammunition_number_t)
        if (ammunition_number <= 0)  and state=="locked":
            ammunition_number = "Bloqué"
        elif (ammunition_number <= 0) and t<0:
             ammunition_number = "Erreur"
        elif (ammunition_number <= 0)  and state=="unlocked":
            ammunition_number = -50
            db_stock(1,0)
        c.execute("INSERT INTO log (date, uid, ammunition_number, ammunition_type) VALUES (?, ?, ?, ?)", (time(), uid, ammunition_number, ammunition_type))
    conn.commit()
    c.close()
    conn.close()

ver = ser.readline().decode().strip()
print("Version=", ver)
while True:
    data=ser.readline().decode().strip()
    if data=="+":
        print("+")
        db_stock(1,1)
    else:
        card_number = data
        print("Carte", card_number)
        ammunition = ser.readline().decode().strip()
        print("Ammo", ammunition)
        ammunition = 50 - int(ammunition)
        db_mun(card_number, 5.56, ammunition,1)


