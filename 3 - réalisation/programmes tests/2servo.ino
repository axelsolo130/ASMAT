#include "Servo.h"

Servo servo1; // création de l'objet "servo1"
Servo servo2; // création de l'objet "servo2"

void setup() {
   servo1.attach(2); // attache le servo au pin spécifié
   servo2.attach(3); // attache le servo au pin spécifié
}

void loop() {
  
   servo1.write(90); // demande au servo de se déplacer à cette position
   servo2.write(110); // demande au servo de se déplacer à cette position
   
   delay(1000); // attend 1000 ms entre changement de position

   servo1.write(180); // demande au servo de se déplacer à cette position
   servo2.write(50); // demande au servo de se déplacer à cette position
   
   delay(1000); // attend 1000 ms entre changement de position


   
}
