from pi_pico_lcd_rgb_grove import * #écran LCD
from pi_pico_nfc_grove import * #lecteur carte
from machine import Pin, I2C, PWM, UART
from time import sleep
import random
'''Initialisation lecteur carte'''
uart0 = UART(0, baudrate=115200)
uart0.init(115200, bits=8, parity=None, stop=1, tx=Pin(0), rx=Pin(1) )
nfc = Pn532_uart(uart0)
nfc.SAM_configuration()
'''Initialisation écran LCD'''
i2c0 = I2C(0, scl=Pin(9), sda=Pin(8), freq=400000)
lcd = Lcd_rgb_grove(i2c0)
lcd.clear()
'''Initialisation Buzzer'''
pwm = PWM(Pin(27))
pwm.freq(1000)


def poser():
    lcd.clear()
    lcd.color(255, 255, 255)
    lcd.setCursor(3, 0)
    lcd.write('Poser etui')
    pwm.duty_u16(0)
def chargement():
    lcd.clear()
    lcd.color(0, 0, 255)
    lcd.setCursor(2, 0)
    lcd.write('Patientez...')
    pwm.duty_u16(0)
def erreur():
    lcd.clear()
    lcd.color(255, 0, 0)
    lcd.setCursor(0, 0)
    lcd.write('Il manque ')
    x=random.randint(1,50)
    lcd.write(x)
    lcd.setCursor(0, 1)
    lcd.write('cartouche(s) !')
    pwm.duty_u16(1000)
def valide():
    lcd.clear()
    lcd.color(0, 255, 0)
    lcd.setCursor(1, 0)
    lcd.write('Pesee valide !')
    pwm.duty_u16(0)
def read():
    uid = None
    while uid is None:   
        uid = nfc.read_passive_target(timeout=500)
    uid_hex = ''.join(['{:02x}'.format(i) for i in uid])
    #print(uid," - ",uid_hex) 
    if uid_hex== "9a72b915":
        last_user=["Gabriel", "Babin", "06XXXXXXXX"]
    elif uid_hex== "596bf36e":
        last_user=["Axel", "Lautonne", "06XXXXXXXX"]   
    else:
        last_user= ["Inconnu", "Inconnu", "Inconnu"]
    return(last_user)
    
try:
    print("start")
    while True:
        read()
        lcd.clear()
        lcd.color(0, 255, 0)
        lcd.setCursor(1, 0)
        print(read())
        lcd.write(last_user[0])
        pwm.duty_u16(0)
         
        
except KeyboardInterrupt:
    print("end")