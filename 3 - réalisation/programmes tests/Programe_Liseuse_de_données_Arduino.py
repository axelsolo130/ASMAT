# Créé par Swann.TRICOIRE, le 29/02/2024 en Python 3.7
import serial
import time

port = 'COM8'
baud_rate = 9600

ser = serial.Serial(port, baud_rate)

time.sleep(2)

while True:
        line = ser.readline().decode().strip()

        print(line)