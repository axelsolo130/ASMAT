int inputPin = 3;               // Connecter le capteur à la broche d'entrée 3
int N = 0;                      // Variable à incrémenter

void setup() {
  Serial.begin(9600);           // Initialiser le port série
  pinMode(inputPin, INPUT);     // Déclarer le microrupteur comme entrée
}

void loop(){
  int val = digitalRead(inputPin);  // Lire la valeur d'entrée
  if (val == HIGH) {                // Vérifier si l'entrée est HIGH
    delay(50);                      // Attendre un court instant pour éviter les rebonds
  } else {
    N++;                            // Incrémenter la variable N
    Serial.print("N = ");           // Afficher la valeur de N
    Serial.println(N);
    delay(1000);                     // Attendre un peu pour éviter les rebonds
  }
}
