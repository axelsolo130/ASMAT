#include "HX711.h"
#include <SPI.h>
#include <MFRC522.h>
#include <rgb_lcd.h>
#include <Servo.h>

const int LOADCELL_DOUT_PIN_1 = 4;
const int LOADCELL_SCK_PIN_1 = 3;
const int LOADCELL_DOUT_PIN_2 = 7;
const int LOADCELL_SCK_PIN_2 = 6;
int inputPin = 3;               // Connecter le capteur à la broche d'entrée 3
#define RST_PIN 9
#define SS_PIN 10
#define BUZZER_PIN 4
#define SERVO_PIN 5             // Choisir la broche appropriée pour le servo

MFRC522 mfrc522(SS_PIN, RST_PIN);
HX711 scale1;
HX711 scale2;
rgb_lcd lcd;
Servo servoMotor;               // Déclaration de l'objet pour le servomoteur

const float calibration_factor1 = -7050; //-7050 cette variable à régler selon le capteur de poids
const float calibration_factor2 = -7050; //-7050 cette variable à régler selon le capteur de poids

String status;

void setup() {
  Serial.begin(9600);
  pinMode(inputPin, INPUT);     // Déclarer le microrupteur comme entrée
  while (!Serial);
  SPI.begin();
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial(); // Affichage des données de la bibliothèque

  lcd.begin(16, 2); // Initialiser l'écran avec 16 colonnes et 2 lignes
  lcd.setRGB(0, 255, 0); // Définir la couleur de fond (vert)

  scale1.begin(LOADCELL_DOUT_PIN_1, LOADCELL_SCK_PIN_1);
  scale2.begin(LOADCELL_DOUT_PIN_2, LOADCELL_SCK_PIN_2);

  scale1.set_scale();
  scale2.set_scale();

  scale1.tare(); // Réinitialise la balance à 0
  scale2.tare(); // Réinitialise la balance à 0

  long zero_factor1 = scale1.read_average(); // Lecture de référence pour l'échelle 1
  long zero_factor2 = scale2.read_average(); // Lecture de référence pour l'échelle 2

  servoMotor.attach(SERVO_PIN);  // Attacher le servomoteur à la broche
  servoMotor.write(0);           // Positionner le servomoteur à 0 degré
}

float lecture_poids(HX711 scale, float calibration_factor) {
  scale.set_scale(calibration_factor); // Ajuste selon ce facteur d'étalonnage
  return scale.get_units();
}

void affichage_info(float poids_moyen) {
}

void loop() {
  float poids1 = lecture_poids(scale1, calibration_factor1);
  float poids2 = lecture_poids(scale2, calibration_factor2);

  float poids_moyen = (poids1 + poids2) / 2.0 - 0.220;

  int cartouches_manquantes = 10;

  lcd.clear();
  lcd.setCursor(0, 0);

  if (mfrc522.PICC_IsNewCardPresent()) {
    if (mfrc522.PICC_ReadCardSerial()) {
      String UID = "";
      for (byte i = 0; i < mfrc522.uid.size; ++i) {
        UID += String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
        UID += String(mfrc522.uid.uidByte[i], HEX);
      }
      Serial.println(" " + UID);

      if (poids_moyen <= 0) {
        cartouches_manquantes = 10;
      } else {
        cartouches_manquantes = int(10 - poids_moyen / 0.162);
        status = "manque " + String(cartouches_manquantes) + " cartouches";
        digitalWrite(BUZZER_PIN, HIGH);
        delay(1000);
        digitalWrite(BUZZER_PIN, LOW);
        Serial.println(cartouches_manquantes);
      }

      if (cartouches_manquantes <= 0) {
        Serial.println("il y a tout");
        digitalWrite(BUZZER_PIN, LOW);
        status = "tout est OK";
      }
      
      if (Serial.available() > 0) { 
        String data = Serial.readStringUntil('\n');
        String state = Serial.readStringUntil('\n');

        lcd.print("Bonjour ");
        lcd.print(data);
        lcd.setCursor(0, 1);
        lcd.print(status);

        if (data == "locked" || data == "unlocked") {
          // Lire la ligne suivante du port série
          String nextState = Serial.readStringUntil('\n');

          if (state == "unlocked") {
            servoMotor.write(0); // Positionner le servomoteur à 0 degré (position verrouillée)
          } else {
            servoMotor.write(90); // Positionner le servomoteur à 90 degrés (position déverrouillée)
            delay(5000);          // Attendre 5 secondes
            servoMotor.write(0);  // Revenir à la position verrouillée
          }
        } 
        delay(1000);
      }
    }
  }

  int val = digitalRead(inputPin);
  if (val == LOW) {
  } 
  else {
    Serial.println("+");
  }
}