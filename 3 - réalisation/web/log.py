import sqlite3
import datetime

def temps():
    time = datetime.datetime.now()
    time= time.strftime("%Y-%m-%d %H:%M:%S")
    return f"{time}"

def db_mun(uid, ammunition_type, ammunition_number):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT last_name, first_name FROM whitelist WHERE uid=?", (uid,))
    resultat = c.fetchone()
    Nom, Prenom = resultat
    utilisateur = Prenom +" "+ Nom
    c.execute("SELECT SUM(CASE WHEN ammunition_number='+50' THEN 1 ELSE 0 END) as count_plus, SUM(CASE WHEN ammunition_number='-50' THEN 1 ELSE 0 END) as count_minus FROM log WHERE uid=?", (uid,))
    test = c.fetchone()
    count_plus, count_minus = test
    if count_plus==None: count_plus=0
    if count_minus==None: count_minus=0
    if not((ammunition_number=="+50" and (count_plus+1) == count_minus) or (ammunition_number=="-50" and count_plus == count_minus) and resultat !=None):
        ammunition_number="Erreur"
    c.execute("INSERT INTO log (date, uid, user, ammunition_number, ammunition_type) VALUES (?, ?, ?, ?, ?)", (temps(), uid, utilisateur, ammunition_number, ammunition_type))
        
     
    conn.commit()
    c.close()
    conn.close()
db_mun(1, "5.56", "+50")