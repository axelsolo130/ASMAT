from flask import Flask, render_template, request, redirect, url_for, abort
import sqlite3
import datetime

app = Flask(__name__)
DB_PATH = 'asmat.db'

def time():
    time = datetime.datetime.now()
    time= time.strftime("%Y-%m-%d %H:%M:%S")
    return time

def last_update():
    last_update = datetime.datetime.now()
    last_update = last_update.strftime("Mis à jour à %H:%M le %d/%m/%Y")
    return last_update

def favourite_ammunition_type(uid):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT ammunition_type, COUNT(*) AS count FROM log WHERE uid=? AND ammunition_number != 'Erreur' GROUP BY ammunition_type ORDER BY count DESC LIMIT 1", (uid,))
    result=c.fetchone()
    if result:
        favourite_ammunition_type = result[0]
    else:
        favourite_ammunition_type="_"
    conn.commit()
    c.close()
    conn.close()
    return favourite_ammunition_type

def visit_count(uid):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT COUNT(*) FROM log WHERE uid=? AND ammunition_number=?", (uid, "-50"))
    result= c.fetchone()
    visit_count=result[0]
    conn.commit()
    c.close()
    conn.close()
    return visit_count

def ammunition_fired_count(uid):
    ammunition_fired_count=visit_count(uid)*50
    return ammunition_fired_count

def returned_shell_count(uid):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT CAST(ammunition_number AS INTEGER) FROM log WHERE uid=? AND CAST(ammunition_number AS INTEGER) > 0", (uid,))
    result= c.fetchall()
    returned_shell_count = sum(result[0] for result in result)
    conn.commit()
    c.close()
    conn.close()
    return returned_shell_count   
    
def last_date(uid):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT date FROM log WHERE uid=? ORDER BY date DESC LIMIT 1", (uid,))
    result=c.fetchone()
    if result:
        last_date=result[0]
    else:
        last_date="_"
    conn.commit()
    c.close()
    conn.close()
    return last_date

def user_state(uid):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT ammunition_number FROM log WHERE uid=? ORDER BY date DESC LIMIT 1", (uid,))
    ammunition_number= c.fetchone()
    if ammunition_number:
        if ammunition_number[0]=="-50" or ammunition_number=="Erreur":
            user_state="Actif"
        else:
            user_state="Inactif"
    else:
        user_state="Inactif"
    conn.commit()
    c.close()
    conn.close() 
    return user_state

def log_user(uid):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT date, ammunition_number, ammunition_type FROM log WHERE uid=? ORDER BY date DESC", (uid,))
    log_user= c.fetchall()
    conn.commit()
    c.close()
    conn.close()
    return log_user   

@app.route('/')
def index():
    conn = sqlite3.connect(DB_PATH)
    c = conn.cursor()
    c.execute("SELECT * FROM whitelist")
    whitelist= c.fetchall()
    c.execute("SELECT log.date, whitelist.first_name || ' ' || whitelist.last_name AS full_name, log.ammunition_number, log.ammunition_type FROM log INNER JOIN whitelist ON log.uid = whitelist.uid ORDER BY log.date DESC")
    log_general= c.fetchall()
    conn.commit()
    c.close()
    conn.close()
    return render_template('asmat.html', whitelist=whitelist, log_general=log_general, last_update=last_update())

@app.route('/add_user', methods=['POST'])
def add_user():
    card_number = request.form['card_number']
    last_name = request.form['last_name']
    first_name = request.form['first_name']
    phone_number = request.form['phone_number']
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT COUNT(*) FROM whitelist WHERE card_number=?", (card_number,))
    count = c.fetchone()[0]
    if count > 0:
        card_number="Erreur"
    state="unlocked"
    c.execute("INSERT INTO whitelist (card_number, last_name, first_name, phone_number, registration_date, state) VALUES (?, ?, ?, ?, ?, ?)", (card_number, last_name, first_name, phone_number, time(), state))
    conn.commit()
    c.close()
    conn.close()
    return redirect(url_for('index'))

@app.route('/remove_user', methods=['POST'])
def remove_user():
    uid = request.form['uid']
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("DELETE FROM whitelist WHERE uid=?", (uid,))
    conn.commit()
    c.close()
    conn.close()
    return redirect(url_for('index'))

@app.route('/user/<uid>')
def user(uid):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT card_number, last_name, first_name, phone_number, registration_date, state FROM whitelist WHERE uid=?", (uid,))
    result= c.fetchone()
    card_number, last_name, first_name, phone_number, registration_date, state=result
    print(state)
    conn.commit()
    c.close()
    conn.close()
    return render_template('user.html',
                           uid=uid,
                           card_number=card_number,
                           last_name=last_name,
                           first_name=first_name,
                           phone_number=phone_number,
                           registration_date=registration_date,
                           state=state,
                           last_date=last_date(uid),
                           favourite_ammunition_type=favourite_ammunition_type(uid),
                           visit_count=visit_count(uid),
                           ammunition_fired_count=ammunition_fired_count(uid),
                           returned_shell_count=returned_shell_count(uid),
                           user_state=user_state(uid),
                           log_user=log_user(uid),
                           last_update=last_update(),
                           )

@app.route('/edit_user', methods=['POST'])
def edit_user():
    card_number = request.form['card_number']
    last_name = request.form['last_name']
    first_name = request.form['first_name']
    phone_number = request.form['phone_number']
    uid = request.form['uid']
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT COUNT(*) FROM whitelist WHERE card_number=?", (card_number,))
    count = c.fetchone()[0]
    if count > 1:
        card_number="Erreur"
    c.execute("UPDATE whitelist SET card_number=?, last_name=?, first_name=?, phone_number=? WHERE uid=?", (card_number, last_name, first_name, phone_number, uid))
    conn.commit()
    c.close()
    conn.close()
    return redirect(f'user/{uid}')

@app.route('/state_user', methods=['POST'])
def state_user():
    uid = request.form['uid']
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT state FROM whitelist WHERE uid=?", (uid,))
    result=c.fetchone()[0]
    if result=="locked": state="unlocked"
    else: state="locked"
    c.execute("UPDATE whitelist SET state=? WHERE uid=?", (state, uid,))
    conn.commit()
    c.close()
    conn.close()
    return redirect(f'user/{uid}')

app.run(host="127.0.0.1", port=5000)
