import sqlite3
import datetime
import serial

port= 'COM5'
baud_rate=9600
ser= serial.Serial(port, baud_rate)

def time():
    time = datetime.datetime.now()
    time= time.strftime("%Y-%m-%d %H:%M:%S")
    return time

def db_mun(card_number, ammunition_type, ammunition_number):
    conn = sqlite3.connect('asmat.db')
    c = conn.cursor()
    c.execute("SELECT uid FROM whitelist WHERE card_number=?", (card_number,))
    uid=c.fetchone()
    if uid:
        uid=uid[0]
        c.execute("SELECT state FROM whitelist WHERE card_number=?", (card_number,))
        result=c.fetchone()[0]
        print(result)
        if result=="unlocked":
            ammunition_number=-50
            print(ammunition_number)
        if ammunition_number<=0 and result=="locked":
            ammunition_number="Erreur"
        c.execute("INSERT INTO log (date, uid, ammunition_number, ammunition_type) VALUES (?, ?, ?, ?)", (time(), uid, ammunition_number, ammunition_type))
        c.execute("SELECT CAST(ammunition_number AS INTEGER) FROM log WHERE uid=?", (uid,))
        ammunition_number_t = c.fetchall()
        t = sum(int(ammunition_number[0]) for ammunition_number in ammunition_number_t)
        if t>=0: state="unlocked"
        else: state="locked"
        c.execute("UPDATE whitelist SET state=? WHERE uid=?", (state, uid,))
    conn.commit()
    c.close()
    conn.close()



ver= ser.readline().decode().strip()
print("Version=", ver)
while True:
    card_number=ser.readline().decode().strip()
    print("card_number=", card_number)
    ammunition=ser.readline().decode().strip()
    ammunition=50-int(ammunition)
    db_mun(card_number, 5.56, ammunition)